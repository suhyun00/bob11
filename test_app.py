from flask import Flask, request
import app, unittest
import requests

#app = Flask(__name__)

class test(unittest.TestCase):
    def test_add(self):
        answer = requests.get("http://13.209.15.210:8067/add?a=1&b=2")
        result = int(answer.text)
        #print(answer.text)
        self.assertEqual(result, 3)

    def test_sub(self):
        answer = requests.get("http://13.209.15.210:8067/sub?a=1&b=2")
        result = int(answer.text)
        #print(answer.text)
        self.assertEqual(result, -1)


if __name__=="__main__":
    unittest.main()
